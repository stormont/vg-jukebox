package com.voyagegames.jukebox.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.voyagegames.jukebox.modules.Item;

public class ItemTest {

	@Test
	public void match_checksArtistAndTitle() {
		Item<String> lhs = new Item<String>("a", null);
		Item<String> rhs = new Item<String>("a", null);
		Item<String> bad = new Item<String>("b", null);
		assertTrue(lhs.match(rhs));
		assertTrue(!lhs.match(bad));
	}

}
