package com.voyagegames.jukebox.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.voyagegames.jukebox.modules.Ranking;

public class RankingTest {

	@Test
	public void score_isDiffBetweenValues() {
		Ranking r = new Ranking();
		r.up();
		r.up();
		r.up();
		r.down();
		assertTrue(r.score() == 2);
	}

}
