package com.voyagegames.jukebox.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.voyagegames.jukebox.modules.Item;
import com.voyagegames.jukebox.modules.ItemComparator;
import com.voyagegames.jukebox.modules.Ranking;

public class ItemComparatorTest {
	
	@Test
	public void compareTo_comparesRankings() {
		ItemComparator<String> c = new ItemComparator<String>();
		Item<String> lhs = new Item<String>(null, new Ranking());
		Item<String> rhs = new Item<String>(null, new Ranking());
		lhs.ranking.up();
		lhs.ranking.up();
		lhs.ranking.up();
		rhs.ranking.up();
		rhs.ranking.up();
		rhs.ranking.up();
		rhs.ranking.down();
		assertTrue(c.compare(lhs, rhs) < 0);
	}

}
