package com.voyagegames.jukebox.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.voyagegames.jukebox.modules.Item;
import com.voyagegames.jukebox.modules.Jukebox;
import com.voyagegames.jukebox.modules.Ranking;

public class JukeboxTest {

	@Test
	public void enqueue_addsUniqueSongs() {
		Jukebox<String> j = new Jukebox<String>();
		Item<String> a = new Item<String>("A", null);
		Item<String> b = new Item<String>("B", null);
		Item<String> c = new Item<String>("C", null);
		j.enqueue(a);
		j.enqueue(b);
		j.enqueue(c);
		assertTrue(j.items().size() == 3);
	}

	@Test
	public void enqueue_ignoresDuplicateSongs() {
		Jukebox<String> j = new Jukebox<String>();
		Item<String> a = new Item<String>("A", null);
		Item<String> b = new Item<String>("A", null);
		j.enqueue(a);
		j.enqueue(b);
		assertTrue(j.items().size() == 1);
	}

	@Test
	public void enqueue_addsToEndOfList() {
		Jukebox<String> j = new Jukebox<String>();
		Item<String> a = new Item<String>("A", null);
		Item<String> b = new Item<String>("B", null);
		Item<String> c = new Item<String>("C", null);
		j.enqueue(a);
		j.enqueue(b);
		j.enqueue(c);
		assertTrue(j.items().size() == 3);
		assertTrue(j.items().get(0).match(a));
		assertTrue(j.items().get(1).match(b));
		assertTrue(j.items().get(2).match(c));
	}

	@Test
	public void pop_removesHeadOfList() {
		Jukebox<String> j = new Jukebox<String>();
		Item<String> a = new Item<String>("A", null);
		Item<String> b = new Item<String>("B", null);
		Item<String> c = new Item<String>("C", null);
		j.enqueue(a);
		j.enqueue(b);
		j.enqueue(c);
		j.pop();
		assertTrue(j.items().size() == 2);
		assertTrue(j.items().get(0).match(b));
		assertTrue(j.items().get(1).match(c));
	}

	@Test
	public void upRank_ignoresIndexOutOfRange() {
		Jukebox<String> j = new Jukebox<String>();
		Item<String> a = new Item<String>("A", new Ranking());
		Item<String> b = new Item<String>("B", new Ranking());
		Item<String> c = new Item<String>("C", new Ranking());
		j.enqueue(a);
		j.enqueue(b);
		j.enqueue(c);
		j.upRank(4);
		j.upRank(-1);
		assertTrue(j.items().size() == 3);
		assertTrue(j.items().get(0).match(a));
		assertTrue(j.items().get(1).match(b));
		assertTrue(j.items().get(2).match(c));
	}

	@Test
	public void downRank_ignoresIndexOutOfRange() {
		Jukebox<String> j = new Jukebox<String>();
		Item<String> a = new Item<String>("A", new Ranking());
		Item<String> b = new Item<String>("B", new Ranking());
		Item<String> c = new Item<String>("C", new Ranking());
		j.enqueue(a);
		j.enqueue(b);
		j.enqueue(c);
		j.downRank(4);
		j.downRank(-1);
		assertTrue(j.items().size() == 3);
		assertTrue(j.items().get(0).match(a));
		assertTrue(j.items().get(1).match(b));
		assertTrue(j.items().get(2).match(c));
	}

	@Test
	public void upRank_resortsList() {
		Jukebox<String> j = new Jukebox<String>();
		Item<String> a = new Item<String>("A", new Ranking());
		Item<String> b = new Item<String>("B", new Ranking());
		Item<String> c = new Item<String>("C", new Ranking());
		j.enqueue(a);
		j.enqueue(b);
		j.enqueue(c);
		j.upRank(1);
		assertTrue(j.items().size() == 3);
		assertTrue(j.items().get(0).match(b));
		assertTrue(j.items().get(1).match(a));
		assertTrue(j.items().get(2).match(c));
	}

	@Test
	public void downRank_resortsList() {
		Jukebox<String> j = new Jukebox<String>();
		Item<String> a = new Item<String>("A", new Ranking());
		Item<String> b = new Item<String>("B", new Ranking());
		Item<String> c = new Item<String>("C", new Ranking());
		j.enqueue(a);
		j.enqueue(b);
		j.enqueue(c);
		j.downRank(0);
		assertTrue(j.items().size() == 3);
		assertTrue(j.items().get(0).match(b));
		assertTrue(j.items().get(1).match(c));
		assertTrue(j.items().get(2).match(a));
	}

}
