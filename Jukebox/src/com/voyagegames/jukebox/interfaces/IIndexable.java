package com.voyagegames.jukebox.interfaces;

public interface IIndexable {
	
	// Gets the current index value
	int index();
	
	// Sets the current index value
	void setIndex(int index);

}
