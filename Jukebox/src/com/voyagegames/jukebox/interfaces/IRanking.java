package com.voyagegames.jukebox.interfaces;

public interface IRanking {
	
	// Increments the "up" votes for a ranking
	void up();
	
	// Increments the "down" votes for a ranking
	void down();
	
	// Scores the ranking based upon current up and down votes
	int score();

}
