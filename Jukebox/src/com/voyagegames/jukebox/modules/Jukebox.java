package com.voyagegames.jukebox.modules;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Jukebox <T> {
	
	final private List<Item<T>> mItems = new ArrayList<Item<T>>();
	
	public List<Item<T>> items() {
		return Collections.unmodifiableList(mItems);
	}
	
	public void enqueue(final Item<T> item) {
		for (Item<T> s : mItems) {
			if (s.match(item)) {
				return;
			}
		}
		
		mItems.add(item);
	}
	
	public void pop() {
		mItems.remove(0);
	}
	
	public void upRank(final int index) {
		if (index < 0 || index >= mItems.size()) {
			return;
		}
		
		mItems.get(index).ranking.up();
		sortByRanking();
	}
	
	public void downRank(final int index) {
		if (index < 0 || index >= mItems.size()) {
			return;
		}
		
		mItems.get(index).ranking.down();
		sortByRanking();
	}
	
	private void sortByRanking() {
		final ItemComparator<T> c = new ItemComparator<T>();
		Collections.sort(mItems, c);
	}

}
