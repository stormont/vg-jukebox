package com.voyagegames.jukebox.modules;

import java.util.Comparator;

public class ItemComparator <T> implements Comparator<Item<T>> {

	@Override
	public int compare(final Item<T> lhs, final Item<T> rhs) {
		return (rhs.ranking.score() - lhs.ranking.score());
	}

}
