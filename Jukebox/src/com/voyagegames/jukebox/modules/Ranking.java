package com.voyagegames.jukebox.modules;

import com.voyagegames.core.interfaces.ICounter;
import com.voyagegames.core.modules.CounterUp;
import com.voyagegames.jukebox.interfaces.IRanking;

public class Ranking implements IRanking {
	
	private final ICounter mUp = new CounterUp();
	private final ICounter mDown = new CounterUp();

	@Override
	public void up() {
		mUp.up();
	}

	@Override
	public void down() {
		mDown.up();
	}

	@Override
	public int score() {
		return (mUp.value() - mDown.value());
	}

}
