package com.voyagegames.jukebox.modules;

import com.voyagegames.jukebox.interfaces.IRanking;

public class Item <T> {
	
	public final T data;
	public final IRanking ranking;
	
	public Item(final T data, final IRanking ranking) {
		this.data = data;
		this.ranking = ranking;
	}
	
	public boolean match(final Item<T> rhs) {
		return (this.data == rhs.data);
	}

}
